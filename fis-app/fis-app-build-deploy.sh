#!/usr/bin/env bash

NAMESPACE=myproject

oc login -u developer -p developer
oc project ${NAMESPACE}
cd fis-app
mvn -Popenshift -DskipTests fabric8:deploy
