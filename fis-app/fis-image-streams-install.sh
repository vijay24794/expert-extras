#!/usr/bin/env bash

oc login -u system:admin

oc create secret docker-registry imagestreamsecret \
  --docker-server=registry.redhat.io \
  --docker-username=${REDHAT_IO_REGISTRY_USER} \
  --docker-password=${REDHAT_IO_REGISTRY_PASSWORD} \
  --docker-email=${REDHAT_IO_REGISTRY_USER} \
  -n openshift \
  --dry-run -o yaml | oc create -f -

oc delete is fis-java-openshift -n openshift
oc delete is jboss-fuse70-java-openshift -n openshift
oc delete is fis-karaf-openshift -n openshift
oc delete is jboss-fuse70-karaf-openshift -n openshift
oc delete is jboss-fuse70-eap-openshift -n openshift
oc delete is jboss-fuse70-console -n openshift
oc delete is fuse7-java-openshift -n openshift
oc delete is fuse7-karaf-openshift -n openshift
oc delete is fuse7-eap-openshift -n openshift
oc delete is fuse7-console -n openshift
oc delete is apicurito-ui -n openshift
oc delete is fuse-apicurito-generator -n openshift

BASEURL=https://raw.githubusercontent.com/jboss-fuse/application-templates/application-templates-2.1.fuse-730065-redhat-00002
oc create -n openshift -f ${BASEURL}/fis-image-streams.json
