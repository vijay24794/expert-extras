#!/bin/bash

# We use the project, myproject on the MiniShift for the demo.
NAMESPACE=myproject

oc login -u system:admin
oc project ${NAMESPACE}

oc delete configmap prometheus > /dev/null 2>&1
oc delete configmap prometheus-alerts > /dev/null 2>&1
oc delete configmap prometheus-rules > /dev/null 2>&1
oc process -f prometheus-template.yml -p NAMESPACE=${NAMESPACE} | oc delete -f -

oc create configmap prometheus --from-file=prometheus.yaml
oc label configmap prometheus app=prometheus
oc create configmap prometheus-alerts --from-file=alertmanager.yaml
oc label configmap prometheus-alerts app=prometheus
oc create configmap prometheus-rules --from-file=rules
oc label configmap prometheus-rules app=prometheus
oc process -f prometheus-template.yml -p NAMESPACE=${NAMESPACE} | oc create -f -
oc adm policy add-cluster-role-to-user system:auth-delegator -z prometheus -n ${NAMESPACE}
echo "wait 1 minute for completing the Prometheus image downloading..."
sleep 60
open https://$(oc get route prometheus --template={{.spec.host}} -n ${NAMESPACE})


