#!/bin/bash

NAMESPACE=myproject

# We use the project, myproject on the Minishift.
oc login -u system:admin
oc project ${NAMESPACE}

oc delete configmap grafana-datasources > /dev/null 2>&1
oc process -f grafana-template.yml -p NAMESPACE=${NAMESPACE} | oc delete -f -

PROMETHEUS_URL=$(oc get svc prometheus -o template --template='https://{{.metadata.name}}.{{.metadata.namespace}}.svc{{println}}')
GRAFANA_TOKEN=$(oc sa get-token prometheus)
oc process -f grafana-datasources-template.yml \
  -p PROMETHEUS_URL=${PROMETHEUS_URL} \
  -p GRAFANA_TOKEN=${GRAFANA_TOKEN} \
  | oc create -f -
oc process -f grafana-template.yml -p NAMESPACE=${NAMESPACE} | oc create -f -
echo "wait 1 minute for completing the Grafana image downloading..."
sleep 60
open https://$(oc get route grafana --template={{.spec.host}})
